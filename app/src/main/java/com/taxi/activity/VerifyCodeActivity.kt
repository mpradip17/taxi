package com.taxi.activity

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.taxi.R
import kotlinx.android.synthetic.main.loginmobile_layout.*

class VerifyCodeActivity : AppCompatActivity() , View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verifycode)

        next.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.next -> {

                runOnUiThread {
                    //code that runs in main
                    Handler().postDelayed({
                        ActivityLauncher.startHomeActivity()

                    }, 2000)
                }
            }



        }
    }
}
