package com.taxi.activity

import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import com.taxi.activity.AppIntroActivity
import com.taxi.utils.MyApp

class ActivityLauncher() {
    companion object Factory {
        //  fun create(): Dog = Dog()

        fun startWelcomeActivity() {
            val intent = Intent(MyApp.instance, AppIntroActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            MyApp.instance.startActivity(intent)
        }
        fun startLoginActivity() {
            val intent = Intent(MyApp.instance, LoginIntroActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            MyApp.instance.startActivity(intent)
        }

        fun startVerifyActivity() {
            val intent = Intent(MyApp.instance, VerifyCodeActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            MyApp.instance.startActivity(intent)
        }

        fun startHomeActivity() {
            val intent = Intent(MyApp.instance, HomeActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            MyApp.instance.startActivity(intent)
        }
    }
}