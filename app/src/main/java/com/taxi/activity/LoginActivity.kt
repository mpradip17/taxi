package com.taxi.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.taxi.R
import com.taxi.utils.MyApp
import kotlinx.android.synthetic.main.loginmobile_layout.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private var behavior: BottomSheetBehavior<View>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loginmobile_layout)


        next.setOnClickListener(this)
        contactus.setOnClickListener(this)
        cancel.setOnClickListener(this)
        bottom_sheet.visibility = View.GONE

        //   initBottomSheet()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.next -> {
                hidemyicons()
                showbottomsheet()

                runOnUiThread {
                    //code that runs in main
                    Handler().postDelayed({
                        /* Create an Intent that will start the Menu-Activity. */
                        ActivityLauncher.startVerifyActivity()

                    }, 2000)
                }
            }

            R.id.contactus -> {

                behavior?.state = BottomSheetBehavior.STATE_COLLAPSED
                showmyicons()

            }
            R.id.cancel -> {
                behavior?.state = BottomSheetBehavior.STATE_COLLAPSED
                showmyicons()

            }
        }
    }

    private fun showbottomsheet() {
        bottom_sheet.setVisibility(View.VISIBLE);

    }

    @SuppressLint("RestrictedApi")
    private fun hidemyicons() {
        sample.setVisibility(View.GONE);
        next.setVisibility(View.GONE);
    }

    @SuppressLint("RestrictedApi")
    private fun showmyicons() {
        sample.setVisibility(View.VISIBLE);
        next.setVisibility(View.VISIBLE);
    }

    private fun initBottomSheet() {
        behavior = BottomSheetBehavior.from(bottom_sheet)


         behavior?.peekHeight = 0

    }


}
