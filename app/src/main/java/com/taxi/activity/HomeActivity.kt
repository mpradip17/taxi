package com.taxi.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.taxi.R
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.layout_navigation_drawer.*

class HomeActivity : AppCompatActivity() {
  //  private var navigationView: NavigationView? = null
    private var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        mContext = this@HomeActivity


    /*    navigationView = findViewById(R.id.navigationView) as NavigationView?

        drawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout?
*/
     //   drawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout?

        navigationView!!.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.getride -> {

                }
                R.id.yourrides -> {


                }
                R.id.payment -> {

                }
                R.id.refernearn -> {

                }

                R.id.supprot -> {

                }
                R.id.offer -> {

                }
                R.id.setting -> {

                }
            }

            // Menu item clicked on, and close Drawerlayout
            menuItem.isChecked = true
            drawer_layout!!.closeDrawers()

            true
        }
    }

}
