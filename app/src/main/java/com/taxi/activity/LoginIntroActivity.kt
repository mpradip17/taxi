package com.taxi.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.taxi.R
import com.taxi.utils.MyApp

class LoginIntroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)

        runOnUiThread {
            //code that runs in main
            Handler().postDelayed({
                /* Create an Intent that will start the Menu-Activity. */
                val intent = Intent(MyApp.instance,LoginActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                MyApp.instance.startActivity(intent)
            }, 2000)
        }
    }

}
