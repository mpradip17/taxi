package com.taxi.activity

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.taxi.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        runOnUiThread {
            //code that runs in main
            Handler().postDelayed({
                /* Create an Intent that will start the Menu-Activity. */
                ActivityLauncher.startWelcomeActivity()
                finish()
            }, 3000)
        }

    }

}
